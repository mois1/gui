# MOIS FE (GUI)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0-rc.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.



# `prod` a `dev` mode, zapínejte v `dev`
* prod mode
    * `ng serve --env=prod`
* dev mode
    * `ng serve`



# Build
* `ng build --env=prod`

### Moduly

# Toaster
* https://www.npmjs.com/package/ngx-toastr/v/4.3.0

# Cookie
* https://www.npmjs.com/package/ngx-cookie-service/v/1.0.0

# Axios
* https://www.npmjs.com/package/axios

# Spinner
* https://www.npmjs.com/package/angular2-loaders-css/v/1.0.9
* Nutno přidat css viz. -> style.css pro background

# Grafy - chartJs
* https://www.npmjs.com/package/angular2-chartjs/v/0.5.1

#Drag and drop - wishList
* https://www.npmjs.com/package/ng2-dragula/v/1.5.0

#Tooltip
* https://www.npmjs.com/package/ng2-tooltip-directive/v/2.1.0

#DateTime picker
* https://www.npmjs.com/package/ng-pick-datetime/v/4.0.1
* momentjs: https://momentjs.com/guides/

**MIT License**

Copyright 2020 Jan Krunčík

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
