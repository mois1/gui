import {Injectable} from '@angular/core';

@Injectable()
export default class ChatBotValidationService {

  QUESTION_MIN_LENGTH = 1;
  QUESTION_MAX_LENGTH = 250;

  QUESTION_REG_EXP_ALLOWED_CHARACTERS = '[a-zA-Zá-žÁ-Ž\\d\\w?!,.+¨*\\\\/|\\-\\s(){}\\[\\]\":]';
  QUESTION_REG_EXP = '^' + this.QUESTION_REG_EXP_ALLOWED_CHARACTERS + '{1,' + this.QUESTION_MAX_LENGTH + '}$';

  questionValidation(question) {
    const message = '';

    if (question === undefined || question === null) {
      return 'Vyplňte otázku.';
    }

    const tmpQuestion = question.toString().trim();
    if (question.replace('\\s', '').isEmpty || question.toString().trim().length < this.QUESTION_MIN_LENGTH) {
      return 'Otázka musí obsahovat alespoň ' + this.QUESTION_MIN_LENGTH + ' znaků.';
    }
    if (tmpQuestion.length > this.QUESTION_MAX_LENGTH) {
      return 'Otázka může obsahovat maximálně ' + this.QUESTION_MAX_LENGTH + 'znaků.';
    }

    if (!tmpQuestion.match(this.QUESTION_REG_EXP)) {
      return 'Otázka obsahuje nepovolené znaky.';
    }

    return message;
  }

}
