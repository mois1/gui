import {Injectable} from '@angular/core';

// TODO dodělat validaci (bílé znaky...)
@Injectable()
export default class UserValidationService {

  FIRST_NAME_MIN_LENGTH = 3;
  FIRST_NAME_MAX_LENGTH = 30;
  FIRST_NAME_REGEX = '^[A-ZÁ-Ž][a-zA-ZÁ-Ž]+\\s*$';

  LAST_NAME_MIN_LENGTH = 4;
  LAST_NAME_MAX_LENGTH = 50;
  LAST_NAME_REGEX = '^[A-ZÁ-Ž]+[a-zA-ZÁ-Ž]+\\s*$';

  EMAIL_MIN_LENGTH = 10;
  EMAIL_MAX_LENGTH = 150;
  EMAIL_REGEX = '^[\\w\\-+]+(\\.[\\w\\-]+)*@[A-Za-z\\d\\-]+(\\.[A-Za-z\\d]+)*(\\.[A-Za-z]{2,})$';

  PASSWORD_MIN_LENGTH = 8;
  PASSWORD_MAX_LENGTH = 50;
  PASSWORD_REGEX = '^(?=.*[\\d])(?=.*[a-z])(?=.*[A-Z])(?=.*[á-žÁ-Ž])(?=.*[@#$%^&+=¨ˇ!/\\\\*|\'-;_():<>{}\\[\\]])(?=\\S+$).{8,50}$';

  TELEPHONE_REGEX = '^(\\+\\d{12}$|^\\d{9})$|(\\d{3}\\s*\\d{3}\\s*\\d{3}|\\+\\s*\\d{3}\\s*\\d{3}\\s*\\d{3}\\s*\\d{3})\\s*$';
  TELEPHONE_MIN_LENGTH = 9;
  TELEPHONE_MAX_LENGTH = 30;

  ACCOUNTID_MIN_LENGTH = 3;
  ACCOUNTID_MAX_LENGTH = 15;
  ACCOUNTID_REGEX = '^[\\d]+\\s*$';

  ADDRESS_STREET_MIN_LENGTH = 3;
  ADDRESS_STREET_MAX_LENGTH = 100;
  ADDRESS_STREET_REGEX = '^([a-zA-Zá-žÁ-Ž]+|[a-zA-Zá-žÁ-Ž]+\\s[a-zA-Zá-žÁ-Ž]+|[a-zA-Zá-žÁ-Ž]+[\\s+a-zA-Zá-žÁ-Ž]+)\\s+\\d+\\s*$';

  ADDRESS_CITY_MIN_LENGTH = 2;
  ADDRESS_CITY_MAX_LENGTH = 100;
  ADDRESS_CITY_REGEX = '^([a-zA-Zá-žÁ-Ž]+|[a-zA-Zá-žÁ-Ž]+\\s+[a-zA-Zá-žÁ-Ž]+|[a-zA-Zá-žÁ-Ž]+[\\s+a-zA-Zá-žÁ-Ž]+)$';

  ADDRESS_PSC_REGEX = '^\\d{3}\\s*\\d{2}\\s*';
  ADDRESS_PSC_MIN_LENGTH = 5;
  ADDRESS_PSC_MAX_LENGTH = 6;


  firstNameValidation(firstName): string {
    let message = '';
    const tmpFirstName = firstName.trim();

    if (!tmpFirstName.match(this.FIRST_NAME_REGEX)) {
      message = 'Jméno obsahuje nepovolené znaky. Musí začínát velkým písmenem.';
    }
    if (firstName.replace('\s', '').isEmpty || firstName.trim().length < this.FIRST_NAME_MIN_LENGTH) {
      message = 'Jméno musí obsahovat alespoň ' + this.FIRST_NAME_MIN_LENGTH + ' znaky.';
    }
    if (tmpFirstName.length > this.FIRST_NAME_MAX_LENGTH) {
      message = 'Jméno může obsahovat maximálně ' + this.FIRST_NAME_MAX_LENGTH + ' znaků.';
    }
    return message;
  }


  lastNameValidation(lastName): string {
    let message = '';
    const tmpLastName = lastName.trim();

    if (!tmpLastName.match(this.LAST_NAME_REGEX)) {
      message = 'Příjmení obsahuje nepovolené znaky. Musí začínát velkým písmenem.';
    }
    if (lastName.replace('\s', '').isEmpty || lastName.trim().length < this.LAST_NAME_MIN_LENGTH) {
      message = 'Příjmení musí obsahovat alespoň ' + this.LAST_NAME_MIN_LENGTH + ' znaky.';
    }
    if (tmpLastName.length > this.LAST_NAME_MAX_LENGTH) {
      message = 'Příjmení může obsahovat maximálně ' + this.LAST_NAME_MAX_LENGTH + ' znaků.';
    }
    return message;
  }


  emailValidation(email): string {
    let message = '';
    const tmpEmail = email.trim();

    if (!tmpEmail.match(this.EMAIL_REGEX)) {
      message = 'Email obsahuje nepovolené znaky, nebo je zadán ve špatném formátu.';
    }
    if (email.replace('\s', '').isEmpty || email.trim().length < this.EMAIL_MIN_LENGTH) {
      message = 'Email musí obsahovat alespoň ' + this.EMAIL_MIN_LENGTH + ' znaky.';
    }
    if (tmpEmail.length > this.EMAIL_MAX_LENGTH) {
      message = 'Email může obsahovat maximálně ' + this.EMAIL_MAX_LENGTH + ' znaků.';
    }
    return message;
  }


  passwordValidation(password): string {
    let message = '';
    const tmpPassword = password.trim();

    if (!tmpPassword.match(this.PASSWORD_REGEX)) {
      message = 'Heslo musí obsahovat alespoň 1 číslo, malé písmeno, velké písmeno, ' +
        'a speciální znak, jako \'@#$%^&+=!/\\*|\'-;_():<>{}[]\' atd..';
    }
    if (password.replace('\s', '').isEmpty || password.replace('\s', '').length < this.PASSWORD_MIN_LENGTH) {
      message = 'Heslo musí obsahovat alespoň ' + this.PASSWORD_MIN_LENGTH + ' znaky.';
    }
    if (tmpPassword.length > this.PASSWORD_MAX_LENGTH) {
      message = 'Heslo může obsahovat maximálně ' + this.PASSWORD_MAX_LENGTH + ' znaků.';
    }
    return message;
  }


  accountIdValidation(accountId): string {
    let message = '';

    if (accountId === undefined || accountId === null) {
      return message;
    }

    const tmpAccountId = accountId.toString().replace('\s', '');

    if (tmpAccountId.length === 0) {
      return message;
    } else if (tmpAccountId.length < this.ACCOUNTID_MIN_LENGTH) {// Not necessary
      message = 'Číslo účtu musí obsahovat alespoň ' + this.ACCOUNTID_MIN_LENGTH + ' čísla.';
    } else if (tmpAccountId.length > this.ACCOUNTID_MAX_LENGTH) {
      message = 'Číslo účtu může obsahovat maximálně ' + this.ACCOUNTID_MAX_LENGTH + ' čísel.';
    } else if (!tmpAccountId.match(this.ACCOUNTID_REGEX)) {
      message = 'Číslo účtu obsahuje nepovolené znaky.';
    }
    return message;
  }


  telephoneNumberValidation(telephoneNumber): string {
    let message = '';

    if (telephoneNumber === undefined || telephoneNumber === null) {
      return message;
    }

    const tmpTelephoneNumber = telephoneNumber.toString().replace('\s', '');

    if (tmpTelephoneNumber.toString().length > 0) {
      if (!tmpTelephoneNumber.match(this.TELEPHONE_REGEX)) {
        message = 'Telefonní číslo může být zadáno buď s předvolbou, nebo bez. Tedy 9 či 13 znaků.';
      }
    }

    return message;
  }


  cityValidation(city): string {
    let message = '';
    const tmpCity = city.toString().trim();

    if (!tmpCity.match(this.ADDRESS_CITY_REGEX)) {
      message = 'Město je zadáno ve špatném formátu.';
    }
    if (tmpCity.length < this.ADDRESS_CITY_MIN_LENGTH) {
      message = 'Město musí obsahovat alespoň ' + this.ADDRESS_CITY_MIN_LENGTH + ' znaky.';
    }
    if (tmpCity.length > this.ADDRESS_CITY_MAX_LENGTH) {
      message = 'Město může obsahovat maximálně ' + this.ADDRESS_CITY_MAX_LENGTH + ' znaků.';
    }
    return message;
  }


  streetValidation(street): string {
    let message = '';
    const tmpStreet = street.trim();

    if (!tmpStreet.match(this.ADDRESS_STREET_REGEX)) {
      message = 'Ulice je zadána ve špatném formátu.';
    }
    if (street.trim().length < this.ADDRESS_STREET_MIN_LENGTH) {
      message = 'Ulice musí obsahovat alespoň ' + this.ADDRESS_STREET_MIN_LENGTH + ' znaky.';
    }
    if (tmpStreet.length > this.ADDRESS_STREET_MAX_LENGTH) {
      message = 'Ulice může obsahovat maximálně ' + this.ADDRESS_STREET_MAX_LENGTH + ' znaků.';
    }
    return message;
  }


  pscValidation(psc): string {
    let message = '';

    if (psc.toString().length === 0) {
      message = 'PSČ musí být zadáno.';
    } else if (!psc.toString().replace('\s', '').match(this.ADDRESS_PSC_REGEX)) {
      message = 'PSČ obsahuje nepovolené znaky. Zadejte platné PSČ.';
    }
    return message;

  }

}
