import {Injectable} from '@angular/core';

@Injectable()
export default class TransactionValidationService {

  DESCRIPTION_MAX_LENGTH = 500;
  DESCRIPTION_REG_EXP = '^[a-zA-Zá-žÁ-Ž\\w\\s,.?!_\\-]{0,' + (this.DESCRIPTION_MAX_LENGTH - 1) + '}$';
  NUMBER_REG_EX = '\\d+';

  ACCOUNTID_MIN_LENGTH = 3;
  ACCOUNTID_MAX_LENGTH = 15;
  ACCOUNTID_REGEX = '^[\\d]+\\s*$';

  validateAmount(amount) {
    let message = '';
    if (amount === null || amount === undefined) {
      message = 'částka musí být zadána.';
      return message;
    }
    if (amount < 0) {
      message = 'Částka musí být kladná.';
    }
    if (!amount.match(this.NUMBER_REG_EX)) {
      message = 'Zadejte pouze čísla.';
    }
    return message;
  }

  validateBankCode(bankCode) {
    let message = '';
    if (bankCode === null || bankCode === undefined) {
      message = 'kód banky musí být zadán.';
      return message;
    }
    if (bankCode < 0) {
      message = 'kód banky nesmí být záporné číslo.';
    }
    if (!bankCode.match(this.NUMBER_REG_EX)) {
      message = 'Zadejte pouze čísla.';
    }
    return message;
  }

  validatePartyDescription(partyDescription) {
    let message = '';
    if (partyDescription == null || partyDescription.replace('\\s', '').isEmpty || partyDescription.length === 0) {
      message = 'Popis transakce musí být vyplněn.';
      return message;
    }
    const tmpPartyDescription = partyDescription.trim();
    if (tmpPartyDescription.length > this.DESCRIPTION_MAX_LENGTH) {
      message = 'Popis platby může obsahovat maximálně ' + this.DESCRIPTION_MAX_LENGTH + ' znaků.';
    }
    if (!tmpPartyDescription.match(this.DESCRIPTION_REG_EXP)) {
      message = 'Popis platby obsahuje nepovolené znaky.';
    }
    return message;
  }

  validateSymbols(symbol) {
    let message = '';
    if (symbol === null || symbol === undefined) {
      return message;
    }
    if (!symbol.match(this.NUMBER_REG_EX)) {
      message = 'Symboly mohou obsahovat pouze čísla.';
    }
    return message;
  }

  accountIdValidation(accountId): string {
    let message = '';

    if (accountId === undefined || accountId === null) {
      return 'Číslo účtu je nutné vyplnit.';
    }

    const tmpAccountId = accountId.toString().replace('\s', '');

    if (tmpAccountId.length === 0) {
      return message;
    } else if (tmpAccountId.length < this.ACCOUNTID_MIN_LENGTH) {// Not necessary
      message = 'Číslo účtu musí obsahovat alespoň ' + this.ACCOUNTID_MIN_LENGTH + ' čísla.';
    } else if (tmpAccountId.length > this.ACCOUNTID_MAX_LENGTH) {
      message = 'Číslo účtu může obsahovat maximálně ' + this.ACCOUNTID_MAX_LENGTH + ' čísel.';
    } else if (!tmpAccountId.match(this.ACCOUNTID_REGEX)) {
      message = 'Číslo účtu obsahuje nepovolené znaky.';
    }
    return message;
  }
}
