import {CLIENT_ID, CLIENT_SECRET} from './client';

export const environment = {
  production: true,
  url_financial_planning_BE: '',
  urlChatBot: '',
  client_id: CLIENT_ID,
  client_secret: CLIENT_SECRET
};
